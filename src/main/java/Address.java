import java.util.ArrayList;

/**
 * Created by dogor on 08.04.2017.
 */
public class Address {

    private ArrayList<String> country;
    private ArrayList<String> region;
    private ArrayList<String> city;
    private ArrayList<String> street;
    private ArrayList<String> houseId;
    private ArrayList<String> houseSubId;
    private ArrayList<String> doorId;
    private int i = -1;
    private int count;



    public Address(){

        country = new ArrayList<>();
        region = new ArrayList<>();
        city = new ArrayList<>();
        street = new ArrayList<>();
        houseId = new ArrayList<>();
        houseSubId = new ArrayList<>();
        doorId = new ArrayList<>();

    }


    public void fillFields(ArrayList<String> fields, int i){

        this.country.add(fields.get(0));
        this.region.add(fields.get(1));
        this.city.add(fields.get(2));
        this.street.add(fields.get(3));
        this.houseId.add(fields.get(4));
        this.houseSubId.add(fields.get(5));
        this.doorId.add(fields.get(6));

        this.count = i;

    }


    public ArrayList<String> getCountry(){

        return this.country;

    }

    public ArrayList<String> getRegion(){

        return this.region;

    }

    public ArrayList<String> getCity(){

        return this.city;

    }

    public ArrayList<String> getHouseId(){

        return this.houseId;

    }

    public ArrayList<String> getHouseSubId(){

        return this.houseSubId;

    }

    public ArrayList<String> getDoorId(){

        return this.doorId;

    }

    public ArrayList<String> getStreet(){

        return this.street;

    }

    @Override

    public String toString(){

        this.i += 1;

        return  "Country: "  + getCountry().get(this.i) + "  Region: " + getRegion().get(this.i)
                + "  City: " + getCity().get(this.i) + "  HouseId: " + getHouseId().get(this.i)
                + "  HouseSubId: " + getHouseSubId().get(this.i) + "  DoorId: " +  getDoorId().get(this.i) + "\n";
    }

}