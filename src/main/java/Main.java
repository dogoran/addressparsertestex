import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by dogor on 08.04.2017.
 */
public class Main {

    public static int lines_count;

    public static void main(String[] args) throws IOException {

        lines_count = 4;

        Address Addres = new Address();

        ParserQ pars = new ParserQ();

        pars.parse(Paths.get("E:\\PROJECTS\\AddressParser2\\src\\main\\resources\\text.txt"),Addres);

        showList(Addres);

    }

    public static void showList(Address addr){

        for (int i = 0; i != (lines_count); i++){
            System.out.println(addr.toString());
        }

    }
}
