import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by dogor on 08.04.2017.
 */
public class ParserQ {

    private String[] result =  new String[6];
    private int i = 0;

    public ParserQ(){}

    public void parse(Path path, Address Addres) throws IOException {

        ArrayList<String> resullt = new ArrayList();

        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

        for (String line: lines) {

            StringTokenizer st = new StringTokenizer(line.toString(), ".,;-");

            this.i += 1;

            while(st.hasMoreTokens()){

                resullt.add(st.nextToken());

            }
            Addres.fillFields(resullt,this.i);
            resullt.clear();

        }



    }


}