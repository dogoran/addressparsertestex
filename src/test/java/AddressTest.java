import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * Created by dogor on 08.04.2017.
 */
public class AddressTest {

    private static int lines_count;
    private Address Addres;


    @Before
    public void init() throws IOException {

        lines_count = 4;

        Addres = new Address();

        ParserQ pars = new ParserQ();

        pars.parse(Paths.get("E:\\PROJECTS\\AddressParser2\\src\\main\\resources\\text.txt"),Addres);

        showList(Addres);

    }

    public static void showList(Address addr){

        for (int i = 0; i != (lines_count); i++){
            System.out.println(addr.toString());
        }

    }


    @Test

    public void countryTest(){

        assertEquals(Addres.getCountry().get(0),"НеСтрана");
        assertEquals(Addres.getCountry().get(1),"USA");
        assertEquals(Addres.getCountry().get(2),"Africa");
        assertEquals(Addres.getCountry().get(3),"UA");

    }


    @Test
    public void regionTest(){

        assertEquals(Addres.getRegion().get(0),"Чубака");
        assertEquals(Addres.getRegion().get(1),"NY");
        assertEquals(Addres.getRegion().get(2),"Tanzania");
        assertEquals(Addres.getRegion().get(3),"DONETSK");

    }

    @Test
    public void cityTest(){

        assertEquals(Addres.getCity().get(0),"Пушкина");
        assertEquals(Addres.getCity().get(1),"WS");
        assertEquals(Addres.getCity().get(2),"KS");
        assertEquals(Addres.getCity().get(3),"DONETSK");

    }

    @Test
    public void houseIdTest(){

        assertEquals(Addres.getHouseId().get(0),"56");
        assertEquals(Addres.getHouseId().get(1),"16");
        assertEquals(Addres.getHouseId().get(2),"4");
        assertEquals(Addres.getHouseId().get(3),"2");

    }

    @Test
    public void houseSubIdTest(){

        assertEquals(Addres.getHouseSubId().get(0),"123");
        assertEquals(Addres.getHouseSubId().get(1),"12");
        assertEquals(Addres.getHouseSubId().get(2),"122");
        assertEquals(Addres.getHouseSubId().get(3),"14");

    }

    @Test
    public void doorIdTest(){

        assertEquals(Addres.getDoorId().get(0),"312");
        assertEquals(Addres.getDoorId().get(1),"2");
        assertEquals(Addres.getDoorId().get(2),"12");
        assertEquals(Addres.getDoorId().get(3),"2");

    }

}
